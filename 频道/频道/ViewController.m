//
//  ViewController.m
//  频道
//
//  Created by mac on 15/6/7.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import "ViewController.h"
#import "Channel.h"
#import "ChannelLabel.h"
@interface ViewController ()<UIScrollViewDelegate,UICollectionViewDataSource,ChannelLabeldelegate>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *layout;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *channelView;
@property (nonatomic, strong) NSArray *channels;
@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupChannels];
    NSLog(@"%@",self.channels);
   
}


//-(UIStatusBarStyle)preferredStatusBarStyle
//{
//
//    return UIStatusBarStyleLightContent;
//}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    _layout.itemSize = self.collectionView.bounds.size;
    _layout.minimumLineSpacing = 0;
    _layout.minimumInteritemSpacing = 0;
    _layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView.pagingEnabled = YES;
    _collectionView.showsHorizontalScrollIndicator = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return _channels.count;
    
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"123" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:((float)arc4random_uniform(256) / 255.0) green:((float)arc4random_uniform(256) / 255.0) blue:((float)arc4random_uniform(256) / 255.0) alpha:1.0];
    return cell;
}



//把标签添加到屏幕
-(void)setupChannels
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    CGFloat margin = 8.0; //间隙
    CGFloat  x =margin; //第一个标签的x值
    CGFloat h =self.channelView.bounds.size.height;
    //循环添加label到channelView
    // 给每个label 添加tag
    int index = 0;
    for(Channel *channel in self.channels)
    {
        ChannelLabel *l = [ChannelLabel channelLabelWithTitle:channel.tname];
        
        l.frame = CGRectMake(x, 0, l.bounds.size.width, h);
        [self.channelView addSubview:l];
        x +=l.bounds.size.width;
        l.delegate = self;
        l.tag = index;
        index++;
    }
    //  设置contentsize
    self.channelView.contentSize = CGSizeMake(x+margin, h);
    /// 默认选中第一项
    self.currentIndex = 0;
    ChannelLabel *firstLabel = _channelView.subviews[0];
    firstLabel.scale = 1;
    
    
}
-(void)channelLabelDidSelected:(ChannelLabel *)channelLabel
{
    _currentIndex = channelLabel.tag;
    NSIndexPath *indexpath=[NSIndexPath indexPathForItem:_currentIndex inSection:0];
    [_collectionView scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ///记录当前在哪一个标签
    ChannelLabel *currentLabel = _channelView.subviews[self.currentIndex];
    ///要在滚动到哪一个标签
    ChannelLabel *nextLabel = nil;
    /// 获取collectionView正在显示的索引数组
    NSArray *indexpaths = [_collectionView indexPathsForVisibleItems];
    /// 遍历数组找到下一个要显示的label
    for (NSIndexPath *path in indexpaths)
    {
        if (path.item != _currentIndex)
        {
        nextLabel = self.channelView.subviews[path.item];
        break;
        }
    }
    NSLog(@"%@-----------%@",currentLabel.text,nextLabel.text);
    ///计算变化了多少
    float nextscale =ABS((float)_collectionView.contentOffset.x / _collectionView.bounds.size.width - _currentIndex);
    float currentscale = 1 - nextscale;
    NSLog(@"%f-----%f",currentscale,nextscale);
    currentLabel.scale = currentscale;
    nextLabel.scale = nextscale;
    NSLog(@"%f",currentscale);
    if (nextLabel==nil)
        return;
    _currentIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    ///重新计算currentIndex
    _currentIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;

    
}
-(NSArray *)channels
{
    if (_channels == nil)
    {
        _channels = [Channel ChannelList];
    }
    return _channels;
}



@end
