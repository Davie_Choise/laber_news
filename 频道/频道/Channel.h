//
//  Channel.h
//  频道
//
//  Created by mac on 15/6/7.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Channel : NSObject
+(NSMutableArray *)ChannelList;
///  频道名称
@property (nonatomic, copy) NSString *tname;
///  频道代号
@property (nonatomic, copy) NSString *tid;
@end
