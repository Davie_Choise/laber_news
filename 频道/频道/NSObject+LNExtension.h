//
//  NSObject+LNExtension.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (LNExtension)

/** 字典转模型 */
+ (instancetype)objectWithDict:(NSDictionary *)dict;

/** 动态获取某个的属性列表 */
+ (NSArray *)properties;
@end
