//
//  ChannelLabel.h
//  频道
//
//  Created by mac on 15/6/7.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChannelLabel;
@protocol ChannelLabeldelegate <NSObject>

-(void)channelLabelDidSelected:(ChannelLabel *)channelLabel;

@end

@interface ChannelLabel : UILabel
@property (nonatomic, assign) float scale;
@property (nonatomic, weak) id<ChannelLabeldelegate> delegate;

+(instancetype)channelLabelWithTitle:(NSString *)title;

@end
