//
//  ChannelLabel.m
//  频道
//
//  Created by mac on 15/6/7.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import "ChannelLabel.h"
#define NomalFontSize 14.0
#define SelectedFontSize 18.0
@implementation ChannelLabel

//返回一个label通过传入一个标题
+(instancetype)channelLabelWithTitle:(NSString *)title
{
    ChannelLabel *l = [[self alloc] init];
    
    l.text =title;
    
    l.font = [UIFont systemFontOfSize:SelectedFontSize];
    
    [l sizeToFit];
    
    l.font = [UIFont systemFontOfSize:NomalFontSize];
    /// 允许用户交互
    l.userInteractionEnabled =  YES;
    return l;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if ([self.delegate respondsToSelector:@selector(channelLabelDidSelected:)])
    {
        [self.delegate channelLabelDidSelected:self];
    }
    
}




-(void)setScale:(float)scale
{
    /// 最大字号和最小字号的放大比例
    float max = SelectedFontSize/NomalFontSize - 1;
    /// 两个要缩放的label对象的以 1 为单位的缩放比例
    float percent = max * scale + 1;
    self.transform = CGAffineTransformMakeScale(percent, percent);
    self.textColor = [UIColor colorWithRed:scale green:0.0 blue:0.0 alpha:1.0];
    
}


@end
