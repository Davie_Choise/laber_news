//
//  Channel.m
//  频道
//
//  Created by mac on 15/6/7.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import "Channel.h"
#import "NSObject+LNExtension.h"
@implementation Channel
+(NSMutableArray *)ChannelList
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"topic_news.json" ofType:nil];
    NSData * data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    NSArray * array = dict[dict.keyEnumerator.nextObject];
    NSMutableArray *channelList = [[NSMutableArray alloc] init];
    [array enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
        Channel *channel = [Channel objectWithDict:obj];
        [channelList addObject:channel];
    }];

    return  [NSMutableArray arrayWithArray:[channelList sortedArrayUsingComparator:^NSComparisonResult(Channel *obj1, Channel *obj2) {
        return [obj1.tid compare:obj2.tid];
    }]];
    
}
@end
